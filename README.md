# Unit Test Case 1

### Clone repository:

\$ git clone <<repository_url>>

Navigate to the cloned repository,

> \$ cd unit-test-case-1

Install package dependencies,

> \$ npm install

### Database Migration:

Set the environment variable for the database,

Create a file named '.env' in the root of the project directory and type in,

> DATABASE_URL = postgres://user_name:password@host:port/db_name

In the terminal run,

> \$ export DATABASE_URL=postgres://user_name:password@host:port/db_name

Migrate database model,

> \$ sequelize db:migrate

### Port number to listen for server:

By default, server is listening at port 3000.

### Run application:

To start the application in development environment,

> \$ npm start

### Run tests

Copy the test directory and its sub-directories from unit-test-solution directory to project root folder

To run tests type in terimnal as,
 >\$ npm test