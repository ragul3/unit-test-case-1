const fastify = require('fastify')();
const cors = require('fastify-cors');

const sampleRoutes = require('./app/routes/sample.routes');

function build() {
  fastify.register(cors, {
    origin: '*',
    methods: 'OPTION, GET, HEAD, PUT, PATCH, POST, DELETE',
    preflightContinue: false,
    optionSuccessStatus: 204
  });

  fastify.register(sampleRoutes);

  return fastify;
};

module.exports = { build };
