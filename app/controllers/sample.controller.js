const SampleService = require('../services/sample.service');

exports.create = (req, reply) => {
  const attributes = req.body.sample;
  SampleService.create(attributes)
    .then((result) => {
      reply.code(201).send(result);
    })
    .catch((error) => {
      reply.send(error);
    });
};

exports.list = (req, reply) => {
  SampleService.find()
    .then((samples) => {
      reply.code(200).send(samples);
    })
    .catch((error) => {
      reply.send(error);
    });
};

exports.update = (req, reply) => {
  const { id } = req.params;
  const attributes = req.body.sample;
  SampleService.update(id, attributes)
    .then((updatedSample) => {
      reply.code(200).send(updatedSample);
    })
    .catch((error) => {
      reply.send(error);
    });
};

exports.delete = (req, reply) => {
  const { ids } = req.query;
  SampleService.destroy(ids)
    .then((message) => {
      reply.code(200).send(message);
    })
    .catch((error) => {
      reply.send(error);
    });
};
