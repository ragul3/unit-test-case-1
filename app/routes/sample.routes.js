const sampleController = require('../controllers/sample.controller');

function samples(fastify, opts, next) {
  fastify.post('/sample', sampleController.create);
  fastify.get('/sample',  sampleController.list);
  fastify.put('/sample/:id', sampleController.update);
  fastify.delete('/sample', sampleController.delete);
  next();
}

module.exports = samples;
