const { Sample } = require('../models');

function find() {
  return Sample.findAll().then(samples => {
    return samples;
  })
}

function create(attributes) {
  return Sample.create(attributes);
}

function update(id, attributes) {
  return Sample.findByPk(id).then((sample) => {
    return sample.update(attributes);
  });
}

function destroy(ids) {
  return Sample.destroy({ where: { id: ids } }).then((message) => {
    if(message === 0) {
      return { message: 'Sample not found'}
    }
    return { message: 'Sample deleted successfully'}
  }).catch((error) => {
    return error;
  });
};

module.exports = {
  create,
  find,
  update,
  destroy
};
