module.exports = (sequelize, DataTypes) => {
  const Sample = sequelize.define(
    'Sample',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: {
            args: [0, 200],
            msg: 'Name length should be less than or equal to 200'
          },
        }
      },
      device_id: {
        type: DataTypes.STRING,
        unique: { args: true, msg: 'Device id should be unique' },
        validate: {
          len: {
            args: [0, 100],
            msg: 'Device id length should be less than or equal to 100'
          },
          notEmpty: { args: [true], msg: 'Device id should not be empty' },
          is: {
            args: [/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$|^\d+$/i],
            msg: 'Device id should be mac address or number'
          }
        }
      },
      description: {
        type: DataTypes.TEXT
      }
    },
    {
      underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  );
  return Sample;
};
