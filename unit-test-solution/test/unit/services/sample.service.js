const { expect } = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const { makeMockModels } = require('sequelize-test-helpers');

describe('app/services/sample/findAll', () => {
  const Sample = {
    findAll: sinon.stub()
  };
  const mockModels = makeMockModels({ Sample });
  const listDatas = [
    {
        "id": "1",
        "name": "Sample1",
        "device_id": "1",
        "description": "Sample1",
        "created_at": "2020-06-26T09:48:59.257Z",
        "updated_at": "2020-06-26T09:48:59.257Z"
    }
  ]

  const SampleService = proxyquire('../../../app/services/sample.service', {
    '../models': mockModels
  });

  let getResult;

  context('list', () => {
    const sampleList = [
      {
          "id": "1",
          "name": "Sample1",
          "device_id": "1",
          "description": "Sample1",
          "created_at": "2020-06-26T09:48:59.257Z",
          "updated_at": "2020-06-26T09:48:59.257Z"
      }
    ]

    before(() => {
      Sample.findAll.resolves(listDatas);
      SampleService.find().then((samples) => {
        getResult = samples;
      });
    });

    after(() => {
      sinon.restore();
    });

    it('list all samples', () => {
      expect(JSON.stringify(getResult)).to.equal(JSON.stringify(sampleList));
    });
  });
});

describe('app/services/sample/create', () => {
  const Sample = {
    create: sinon.stub()
  };
  const mockModels = makeMockModels({ Sample });
  const data = {
    "name": "Sample1",
    "device_id": "1",
    "description": "Sample1"
  }

  const SampleService = proxyquire('../../../app/services/sample.service', {
    '../models': mockModels
  });

  let getResult;

  context('create', () => {
    const createdSample = {
      "id": 1,
      "name": 'Sample01',
      "device_id": '01',
      "description": 'Sample1',
      "created_at": '2020-06-08T12:14:59.715Z',
      "updated_at": '2020-06-08T12:14:59.715Z'
    };

    before(() => {
      Sample.create.resolves(createdSample);
      SampleService.create(data).then((sampleResult) => {
        getResult = sampleResult;
      });
    });

    after(() => {
      sinon.restore();
    });

    it('sample created', () => {
      expect(getResult).to.equal(createdSample);
    });
  });
});

describe('app/services/sample/update', () => {
  const Sample = {
    findByPk: sinon.stub(),
  };
  const mockModels = makeMockModels({ Sample });

  const SampleService = proxyquire('../../../app/services/sample.service', {
    '../models': mockModels
  });

  const id = 1;
  const data = {
    "name": "Sample1",
    "device_id": "1",
    "description": "Sample1"
  };
  const fakeSample = { id, ...data, update: sinon.stub() };
  const updatedSample = {
    "id": "1",
    "name": "Sample01",
    "device_id": "01",
    "description": "Sample01",
    "created_at": "2020-06-26T09:31:36.630Z",
    "updated_at": "2020-06-26T09:31:49.627Z"
  }
  let getResult;

  context('sample Update', () => {
    before(() => {
      Sample.findByPk.resolves(fakeSample);
      fakeSample.update.resolves(updatedSample);
      SampleService.update(id, data).then((sampleResult) => {
        getResult = sampleResult;
      });
    });

    after(() => {
      sinon.restore();
    });

    it('sample is updated', () => {
      expect(getResult).to.equal(updatedSample);
    });

    it('findByPk is called with id 1', () => {
      expect(Sample.findByPk.calledWith(id)).to.equal(true);
    });
  });
});

describe('app/services/sample/delete', () => {
  const Sample = {
    destroy: sinon.stub()
  };
  const mockModels = makeMockModels({ Sample });
  const SampleService = proxyquire('../../../app/services/sample.service', {
    '../models': mockModels
  });

  let getResult;

  context('delete', () => {
    const id = 1;

    before(() => {
      Sample.destroy.resolves({ message: 'Sample deleted successfully'});
      SampleService.destroy(id).then((sampleResult) => {
        getResult = sampleResult;
      });
    });

    after(() => {
      sinon.restore();
    });

    it('sample is deleted', () => {
      expect(JSON.stringify(getResult)).to.equal(JSON.stringify({ message: 'Sample deleted successfully'}));
    });
  });
});
