const {
  sequelize,
  dataTypes,
  checkModelName,
  checkPropertyExists
} = require('sequelize-test-helpers');

const SampleModel = require('../../../../app/models/sample');

describe('app/models/sample', () => {
  const Sample = SampleModel(sequelize, dataTypes);
  const samples = new Sample();

  checkModelName(Sample)('Sample');

  context('properties', () => {
    ;['name', 'device_id', 'description'].forEach(
      checkPropertyExists(samples)
    );
  });
});
